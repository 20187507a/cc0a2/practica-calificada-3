package com.uni.jmalpv.pc3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {


    RadioButton radioButtonBasic, radioButtonScientific, radioButtonProgrammer;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioButtonBasic = findViewById(R.id.radio_button_basic);
        radioButtonScientific = findViewById(R.id.radio_button_scientific);
        radioButtonProgrammer = findViewById(R.id.radio_button_programmer);

        button = findViewById(R.id.button_start);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!radioButtonBasic.isChecked() && !radioButtonScientific.isChecked() && !radioButtonProgrammer.isChecked()) {
                    Snackbar.make(view, R.string.snack_bar_msg, Snackbar.LENGTH_LONG).show();
                    return;
                }

                Intent intent = new Intent(MainActivity.this, CalculatorActivity.class);

                if (radioButtonBasic.isChecked()) {
                    intent.putExtra("BASIC", true);
                    startActivity(intent);
                }
                if (radioButtonScientific.isChecked() || radioButtonProgrammer.isChecked()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle(R.string.dialog_title);
                    builder.setCancelable(false);
                    builder.setMessage(R.string.dialog_msg);
                    builder.setPositiveButton(R.string.dialog_opt_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(MainActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    builder.setNegativeButton(R.string.dialog_opt_no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            moveTaskToBack(true);
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(1);
                        }
                    });
                    builder.create().show();
                }
            }
        });
    }
}