package com.uni.jmalpv.pc3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.widget.Button;
import android.widget.TextView;

public class CalculatorActivity extends AppCompatActivity {

    TextView textViewScore;

    Button buttonNumberZero, buttonNumberOne, buttonNumberTwo, buttonNumberThree, buttonNumberFour, buttonNumberFive, buttonNumberSix, buttonNumberSeven, buttonNumberEight, buttonNumberNine;
    Button buttonBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        textViewScore = findViewById(R.id.text_view_result);

        buttonNumberZero = findViewById(R.id.button_number_zero);
    }
}